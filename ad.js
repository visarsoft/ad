(function() {
  // Create a new H3 element
  var app = document.createElement("div");
  app.setAttribute("id", "adApp");

  // Add the text content
  app.innerText = "Here goes Vue app";

  document.querySelector("body").appendChild(app);

  const imagesList = ['ad1.png', 'ad2.png'];
  const HOST = 'https://ad.visarsoft.de';

  if(typeof window.Vue !== "undefined") {
    // Vue.component("button-counter", {
    //   data: function() {
    //     return {
    //       count: 0
    //     };
    //   },
    //   template:
    //     '<button v-on:click="count++">You clicked me {{ count }} times.</button>'
    // });

    Vue.component("ad-image", {
      props: ["imageName"],
      computed: {
        imagePath() {
          return `${HOST}/assets/${this.imageName}`;
        }
      },
      data() {
        return {
          appStyle: {
            width: "50%"
          }
        };
      },
      methods: {
        onMouseHover(event) {
          this.appStyle.width = "100%";
        },
        onMouseLeave(event) {
          this.appStyle.width = "50%";
        }
      },
      template:
        '<img :style="appStyle" :src="imagePath" :alt="imageName" @mouseover="onMouseHover" @mouseleave="onMouseLeave" />'
    });

    const adImages = () => {
      const arr = []
      imagesList.forEach(image => {
        arr.push(`<ad-image image-name="${image}" />`);
      });
      return arr.join("");
    }

    const app = new Vue({
      el: "#adApp",
      computed: {
        appStyle() {
          return {
            color: "orange",
            border: "1px solid #ddd"
          };
        }
      },

      template: `<div :style="appStyle">${adImages()}</div>`
    });
  } else {
    console.log('Vue not loaded')
  }
})();
